FROM node:9.8-alpine
WORKDIR /app
COPY package.json /app
RUN yarn install --ignore-optional
COPY . /app

EXPOSE 8080

CMD ["node", "index.js"]