'use strict';
var config = require('config-node')();

module.exports = () => {
    if (typeof config.configServer != 'undefined') {
        process.env.CONFIG_CLIENT_API_KEY = config.configServer.apiKey;
        process.env.CONFIG_CLIENT_API_TOKEN = config.configServer.apiToken;
        process.env.CONFIG_CLIENT_URL = config.configServer.url;
    }
    var client = require('nf-config-client');
    var keys = Object.keys(config);

    keys.forEach(async (key) => {
        if (typeof config[key]['nfConfig'] != 'undefined' && config[key]['nfConfig']) {
            config[key] = await client.loadConfig(config[key]['nfConfig'])
        }
    })

    return config;
}